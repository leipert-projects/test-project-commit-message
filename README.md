# Project deploy depending on commit message

This project is a simple project demonstrating deployments depending on commit messages.

This is done using the [rules] syntax and [pre-defined variables][vars]. The rule syntax is super powerful and a lot of variables can be used in it's [expressions].

[rules]: https://docs.gitlab.com/ee/ci/yaml/#rules
[vars]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
[expressions]: https://docs.gitlab.com/ee/ci/jobs/job_control.html#cicd-variable-expressions